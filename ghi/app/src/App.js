import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListShoes from './ListShoes';
import ListHats from './ListHats';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm'

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="listshoes/" element={<ListShoes shoes= {props.shoes}/>} />
          <Route path="hats/" element={<ListHats hats= {props.hats}/>} />
            <Route path="createHat/" element={<HatForm />} />
          <Route path="createashoe/" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
