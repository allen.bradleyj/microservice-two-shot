import {loadShoesandHats} from './index'

async function onDelete (event, hat) {
    event.preventDefault()
    const url = `http://localhost:8090/api/hats/${hat}/`;
    const fetchConfig = {
        method: "delete",
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        loadShoesandHats();
        const newHat = await response.json();
    };
    }


function ListHats(props) {



    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    console.log(hat);
                    return (
                    <tr key={hat.style}>
                        <td>{hat.fabric}</td>
                        <td>{hat.style}</td>
                        <td>{hat.color}</td>
                        <td><img src={hat.picture_url}
                        width="100"
                        height="100"
                        />{}</td>
                        <td>{hat.location.closet_name}</td>
                        <td>
                            <button onClick={(event) => (onDelete(event, hat.id))}>Delete</button>
                        </td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ListHats;
