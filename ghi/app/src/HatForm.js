import React, {useEffect, useState}from "react";

function HatForm(){
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);
    const [picture, setPicture] = useState([]);

    const handleFabricChange = (event) =>{
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleChange = (event) =>{
        const value = event.target.value;
        setStyle(value);
    }
    const handleColorChange = (event) =>{
        const value = event.target.value;
        setColor(value);
    }
    const handleLocationChange = (event) =>{
        const value = event.target.value;
        setLocation(value);
    }

    const handlePictureChange = (event) =>{
        const value = event.target.value;
        setPicture(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.location = location;
        data.picture_url = picture;
        console.log(data);




        const url = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
        },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            event.target.reset();

        }
        }
        const fetchData = async () => {


        const locationUrl = 'http://localhost:8100/api/locations/';
        const fetchLocationConfig = {
            method: 'get',
        }
        const locationResponse = await fetch(locationUrl, fetchLocationConfig);
        if (locationResponse.ok) {
            const data = await locationResponse.json()
            console.log(data);
            setLocations(data.locations);
        };
    };



    useEffect(() => {
        fetchData();
    }, []);

        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat!</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="name" id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={style} onChange={handleStyleChange} placeholder="Style" required type="text" name="manufacturer" id="style" className="form-control"/>
                    <label htmlFor="stye">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={picture} onChange={handlePictureChange} placeholder="Image" required type="url" name="picture" id="picture" className="form-control"/>
                    <label htmlFor="picture">Picture</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                        <option value={location}>Choose a location</option>
                            {locations.map(location => {
                                return (
                        <option key={location.id} value={location.id}>
                            {location.closet_name}</option>
                                )
                            })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
};



export default HatForm;
