# Wardrobify

Team:

* Bradley Allen - Shoes!
* Benjamin Bass - Hats!

## Design

## Shoes microservice

The shoes microservice polls the Wardrobe api every 60 seconds to find new added locations, after that a VO is sent back to shoes_api where it can be accessed through React and used to help fill out the Create A Shoe Form, as well as display where that specific shoe model is located within the Wardrobe.

## Hats microservice

The hat model is the main model that has data for each hat. The locationVO model polls to the wardrobe api and sent back to hats_api. Then React can use api calls to get data needed to create a hat and list the hats.
